﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelDataReader;

namespace ImportFromExcel
{
    class Program
    {
        public static void Main(string[] args)
        {
            var reader = GetExcelReader();
            string sheet = GetTableNames(reader);
            GetFirstRowData(sheet, reader);
        }

        private static IExcelDataReader GetExcelReader()
        {
            FileStream fileStream = File.Open("C:/Users/watenatj/Documents/testing.xlsx", FileMode.Open, FileAccess.Read);

            IExcelDataReader reader = ExcelReaderFactory.CreateReader(fileStream);

            return reader;
        }

        private static string GetTableNames(IExcelDataReader reader)
        {
            var wb = reader.AsDataSet();
            var result = from DataTable s in wb.Tables select s.TableName;

            foreach (var s in result)
            {
                if (s == "Sheet1")
                {
                    return s;
                }
            }

            return null;
        }

        private static void GetFirstRowData(string sheetName, IExcelDataReader excelData)
        {
            var rows = excelData.AsDataSet();
            var ws = rows.Tables[sheetName];

            var result = (from DataRow r in ws.Rows select r).First();

            Console.WriteLine(result[0].ToString());
        }

        private static void DisplayData(IExcelDataReader excelData)
        {
            var data = excelData.AsDataSet();

        }
    }
}
